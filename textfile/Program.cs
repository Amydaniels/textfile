﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace textfile
{
    class Program
    {
        static void Main(string[] args)
        {
            //Random rand = new Random();
            //Console.WriteLine("Randomly double generated is {0}", rand.NextDouble());
            //Console.WriteLine("Randomly int generated is {0}", rand.Next(2, 30));

            //insert lines into a new file
            string[] data = { "This is line 1", "This is line 2", "This is line 3" };
            string path = "C:/users/Amarachi/documents/test.txt";
            File.WriteAllLines(path, data);

            //append lines to an existing file
            string[] append = { "Appended line 1", "Appended line 2", "Appended line 3" };
            File.AppendAllLines(path, append);


            string[] read = File.ReadAllLines("C:/users/Amarachi/documents/test.txt");

            //read from the written file
            Console.WriteLine("The lines in the file are:");
            foreach (string r in read)
            {
                Console.WriteLine(r);
            }
        }
    }
}


        
    

